/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EvelynAnaya
 */

//package socket;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
// paquete que contiene las clases de sockets
import java.net.*;
// paquete que contiene las clases para el manejo de flujo de datos
import java.io.*;

public class Cliente extends JFrame implements ActionListener{
    
    
     Container c;
    JTextField op1,op2,op3;
    JLabel label1,label2,label3,label4;
    JPanel pN,pC,pS;
    JPanel p1,p2,p3,p4;
    JButton boton;
    JTextArea display;
    
    public Cliente()
    {
      
        setTitle("Cliente de socket");
        setLayout(new GridLayout(4,1));
        c= getContentPane();
        c.setBackground(Color.white);
        // creación de las intancias y agregación a los paneles 
        // de las componentes GUI
        // . . . . . . . .  . . . .
        Container contenedor=c;
        contenedor.setLayout(new FlowLayout());
        label1=new JLabel("Introduce porfavor los valores para:");
        pN=new JPanel();
        pN.add(label1);
     
        
        
        label2=new JLabel("Primer numero:");
        contenedor.add(label2);
        op1=new JTextField(7);
        op1.addActionListener(this);
        contenedor.add(op1);
        
        label3=new JLabel("Segundo numero");
        contenedor.add(label3);
        op2=new JTextField(5);
        op2.addActionListener(this);
        contenedor.add(op2);
        
        label4=new JLabel("Registra la ip");
        contenedor.add(label4);
        op3=new JTextField(6);
        op3.addActionListener(this);
        contenedor.add(op3);
        
        boton = new JButton("Sumar");
        contenedor.add(boton);
        
        display=new JTextArea(10,25);
        contenedor.add(display);
        
         pack();
        this.setLocationRelativeTo(null);
        setSize(300,350);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
        
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    
    	s1=Double.parseDouble(op1.getText());
    	s2=Double.parseDouble(op2.getText());
    	System.out.println("Haciendo petición");
    	peticionServidor(s1,s2);
    }
    
    private void peticionServidor(double s1, double s2)
    {
        // declaración de un objecto para el socket cliente
//    ..........
         // declaración de los objetos para el flujo de datos
//         .......
//         ........
        Socket client;
        DataInputStream input;
        DataOutputStream output;
         double suma;    
         String Suma;
       try {
                // creación de la instancia del socket
//                    .............
           //en InetAddres se necesita cambiar la direccion meter la ip
           client=new Socket(op3.getText(),6000);
		    display.setText("Socket Creado....\n");
                // creación de las instancias para el flujo de datos
                   input=new DataInputStream(client.getInputStream());
                   output=new DataOutputStream(client.getOutputStream());
	           display.append("Enviando primer sumando\n");
                  output.writeDouble(s1);
		     display.append("Enviando segundo sumando\n");
                 output.writeDouble(s2);
		     display.append ("El servidor dice....\n\n");
                suma=input.readDouble();
                 Suma= String.valueOf (suma);
	           display.append("El  resultado es: "+ Suma+"\n\n");
                 display.append("Cerrando cliente\n\n");
           client.close();
              }

                catch(IOException e){
                e.printStackTrace();
                }
    }

 public static void main(String args[])
    {
        new Cliente();
        
    }
}
